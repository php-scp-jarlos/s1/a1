<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 1</title>
</head>
<body>
	<h1>Full Address</h1>
	<p><?= getFullAddress("Philippines", "Metro Manila", "Quezon City", "3F Caswynn Bldg., Timog Avenue")?></p>
	<p><?= getFullAddress("Philippines", "Metro Manila", "Makati City", "3F Enzo Bldg., Buendia Avenue") ?></p>

	<h1>Letter-Based Grading</h1>
	<p>87 is equivalent to <?= getLetterGrade(87) ?> </p>
	<p>94 is equivalent to <?= getLetterGrade(94) ?> </p>
	<p>74 is equivalent to <?= getLetterGrade(74) ?> </p>
</body>
</html>